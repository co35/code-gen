package com.icetai.assist.repository;

import com.icetai.assist.entity.MicroService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MicroServiceRepository extends JpaRepository<MicroService, Long> {

  Page<MicroService> findByNameContainingOrDescriptionContainingOrTableNamesContaining(Pageable pageable, String name, String description, String tableNames);

  boolean existsMicroServiceByPort(int port);

  boolean existsMicroServiceByIdentification(String identification);
}

package com.icetai.assist.constant;

public interface KeyConstants {

  String PACKAGE_PATH = "packagePath";
  String CLASS_NAME = "className";
  String LOWERCASE_CLASS_NAME = "lowercaseClassName";

  // 服务名称
  String SERVICE_NAME = "serviceName";
  // 项目标识
  String IDENTIFICATION = "identification";
  String UNIQUE_PREFIX = "uniquePrefix";
  String PORT = "port";
  String PACKAGE_NAME = "packet";
}

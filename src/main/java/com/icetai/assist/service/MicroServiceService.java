package com.icetai.assist.service;

import com.icetai.assist.entity.MicroService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface MicroServiceService {
  MicroService save(MicroService microService);

  Iterable<MicroService> findAll();

    Page<MicroService> findAllPage(Pageable pageable, String keyword);
    Optional<MicroService> findById(long id);

  void delete(MicroService microService);
}

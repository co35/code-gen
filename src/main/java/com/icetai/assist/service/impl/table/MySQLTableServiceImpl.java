package com.icetai.assist.service.impl.table;

import com.icetai.assist.service.BaseTableService;
import org.springframework.stereotype.Service;

@Service("mysql")
public class MySQLTableServiceImpl extends BaseTableService {

  @Override
  protected String getDriverClassName() {
    return "com.mysql.cj.jdbc.Driver";
  }
}

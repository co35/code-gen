package com.icetai.assist.service.impl;

import com.icetai.assist.entity.MicroService;
import com.icetai.assist.model.Table;
import com.icetai.assist.model.TableItem;
import com.icetai.assist.model.TemplateContext;
import com.icetai.assist.service.GeneratorService;
import com.icetai.assist.service.TableService;
import com.icetai.assist.util.SpringContextUtils;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class GeneratorServiceImpl implements GeneratorService {

  private static final Logger log = LoggerFactory.getLogger(GeneratorServiceImpl.class);

  @Value("${generator.template.base-path:}")
  private String templateBasePath;

  @Value("${generator.template.system-path:}")
  private String templateOutputPath;

  @Value("${generator.template.java-paths:}")
  private String templateOutputPaths;

  @Value("${generator.datasource.type:mysql}")
  private String datasourceType;

  @Override
  public void generateZip(
      MicroService service,
      String url,
      String username,
      String password,
      String[] tableNames,
      String zipPath) {
    TableItem[] tableItems = new TableItem[tableNames.length];
    for (int i = 0; i < tableNames.length; i++) {
      tableItems[i] = new TableItem(tableNames[i]);
    }
    generateZip(service, url, username, password, tableItems, zipPath);
  }

  @Override
  public void generateZip(
      MicroService service,
      String url,
      String username,
      String password,
      String[] tableNames,
      ZipOutputStream zos) {
    TableItem[] tableItems = new TableItem[tableNames.length];
    for (int i = 0; i < tableNames.length; i++) {
      tableItems[i] = new TableItem(tableNames[i]);
    }
    generateZip(service, url, username, password, tableItems, zos);
  }

  @Override
  public void generateZip(
      MicroService service,
      String url,
      String username,
      String password,
      TableItem[] tableItems,
      String zipPath) {
    try (FileOutputStream fos = new FileOutputStream(zipPath)) {
      ZipOutputStream zos = new ZipOutputStream(fos);
      generateZip(service, url, username, password, tableItems, zos);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void generateZip(
      MicroService service,
      String url,
      String username,
      String password,
      TableItem[] tableItems,
      ZipOutputStream zos) {
    TableService tableService = SpringContextUtils.getBean(datasourceType, TableService.class);

    try {
      generatorBasicCode(TemplateContext.newBuilder(service).build(), zos);
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      for (TableItem item : tableItems) {
        Table table = tableService.getTable(url, username, password, item.getTableName());
        if (table == null) {
          log.warn("表[{}] 信息查询失败", item.getTableName());
          continue;
        }
        generatorCode(
            TemplateContext.newBuilder(service)
                .templateVariables(item.getTemplateVariables())
                .table(table)
                .dynamicPathVariables(item.getDynamicPathVariables())
                .build(),
            zos);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      zos.closeEntry();
      zos.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static String replace(String pattern, Map<String, String> context) throws Exception {
    char[] patternChars = pattern.toCharArray();
    StringBuilder valueBuffer = new StringBuilder();
    StringBuilder variableNameBuffer = null;
    boolean inVariable = false;
    for (int i = 0; i < patternChars.length; ++i) {
      if (!inVariable && patternChars[i] == '{') {
        inVariable = true;
        variableNameBuffer = new StringBuilder();
        continue;
      }
      if (inVariable && patternChars[i] == '}') {
        inVariable = false;
        String variable = context.get(variableNameBuffer.toString());
        valueBuffer.append(variable == null ? "null" : variable);
        variableNameBuffer = null;
        continue;
      }
      if (patternChars[i] == '\\' && ++i == patternChars.length) {
        throw new Exception("Missing characters after '\\'.");
      }
      StringBuilder activeBuffer = inVariable ? variableNameBuffer : valueBuffer;
      activeBuffer.append(patternChars[i]);
    }
    if (variableNameBuffer != null) {
      throw new Exception("End missing }.");
    }
    return valueBuffer.toString();
  }

  private void generatorCode(TemplateContext context, ZipOutputStream zos) {
    Properties prop = new Properties();
    prop.put(
        "file.resource.loader.class",
        "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    Velocity.init(prop);
    VelocityContext velocityContext = new VelocityContext(context.toMap());

    Map<String, String> outputPathMap = parseTemplateOutputPaths(context);
    for (Map.Entry<String, String> entry : outputPathMap.entrySet()) {
      Template template = Velocity.getTemplate(entry.getKey(), "UTF-8");
      try (StringWriter writer = new StringWriter()) {
        template.merge(velocityContext, writer);
        zos.putNextEntry(new ZipEntry(entry.getValue()));
        IOUtils.write(writer.toString(), zos, "UTF-8");
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  // java 代码的生成目录
  private Map<String, String> parseTemplateOutputPaths(TemplateContext context) {
    String[] rows = templateOutputPaths.split("\n");
    Map<String, String> outputPathMap = new HashMap<>();
    for (String row : rows) {
      int index = row.indexOf(":");
      if (index == -1) {
        continue;
      }
      String fileName = row.substring(0, index).trim();
      try {
        String path = replace(row.substring(index + 1).trim(), context.getDynamicPathVariables());
        outputPathMap.put(templateBasePath + "/" + fileName, path);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return outputPathMap;
  }

  private void generatorBasicCode(TemplateContext context, ZipOutputStream zos) {
    Properties prop = new Properties();
    prop.put(
        "file.resource.loader.class",
        "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    Velocity.init(prop);
    VelocityContext velocityContext = new VelocityContext(context.toMap());

    Map<String, String> outputPathMap = parseTemplateOutputPath(context);
    for (Map.Entry<String, String> entry : outputPathMap.entrySet()) {
      Template template = Velocity.getTemplate(entry.getKey(), "UTF-8");
      try (StringWriter writer = new StringWriter()) {
        template.merge(velocityContext, writer);
        zos.putNextEntry(new ZipEntry(entry.getValue()));
        IOUtils.write(writer.toString(), zos, "UTF-8");
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private Map<String, String> parseTemplateOutputPath(TemplateContext context) {
    String[] rows = templateOutputPath.split("\n");
    Map<String, String> outputPathMap = new HashMap<>();
    for (String row : rows) {
      int index = row.indexOf(":");
      if (index == -1) {
        continue;
      }
      String fileName = row.substring(0, index).trim();
      try {
        String path = replace(row.substring(index + 1).trim(), context.getDynamicPathVariables());
        outputPathMap.put(templateBasePath + "/" + fileName, path);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return outputPathMap;
  }
}

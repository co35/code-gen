package com.icetai.assist.service.impl;

import com.icetai.assist.entity.MicroService;
import com.icetai.assist.repository.MicroServiceRepository;
import com.icetai.assist.service.MicroServiceService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class MicroServiceServiceImpl implements MicroServiceService {

  @Resource MicroServiceRepository microServiceRepository;

  @Override
  public MicroService save(MicroService microService) {
    return microServiceRepository.save(microService);
  }

  @Override
  public Iterable<MicroService> findAll() {
    return microServiceRepository.findAll();
  }

  @Override
  public Page<MicroService> findAllPage(Pageable pageable, String keyword) {
    if (StringUtils.isNotBlank(keyword))
      return microServiceRepository
          .findByNameContainingOrDescriptionContainingOrTableNamesContaining(
              pageable, keyword, keyword, keyword);
    else return microServiceRepository.findAll(pageable);
  }

  @Override
  public Optional<MicroService> findById(long id) {
    return microServiceRepository.findById(id);
  }

  @Override
  public void delete(MicroService microService) {
    microServiceRepository.delete(microService);
  }
}

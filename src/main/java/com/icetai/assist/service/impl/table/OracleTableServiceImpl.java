package com.icetai.assist.service.impl.table;

import com.icetai.assist.service.BaseTableService;
import org.springframework.stereotype.Service;

@Service("oracle")
public class OracleTableServiceImpl extends BaseTableService {

  @Override
  protected String getDriverClassName() {
    return "oracle.jdbc.driver.OracleDriver";
  }
}

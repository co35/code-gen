package com.icetai.assist.service;

import com.icetai.assist.model.Table;

public interface TableService {

  Table getTable(String url, String username, String password, String tableName) throws Exception;
}

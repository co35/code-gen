package com.icetai.assist.service;

import com.icetai.assist.entity.MicroService;
import com.icetai.assist.model.TableItem;

import java.util.zip.ZipOutputStream;

public interface GeneratorService {

  void generateZip(MicroService service, String url, String username, String password, String[] tableNames, String zipPath);

  void generateZip(MicroService service, String url, String username, String password, String[] tableNames, ZipOutputStream zos);

  void generateZip(MicroService service, String url, String username, String password, TableItem[] tableItems, String zipPath);

  void generateZip(MicroService service, String url, String username, String password, TableItem[] tableItems, ZipOutputStream zos);
}

package com.icetai.assist.controller;

import com.icetai.assist.annotation.OnCreate;
import com.icetai.assist.annotation.OnUpdate;
import com.icetai.assist.entity.MicroService;
import com.icetai.assist.service.GeneratorService;
import com.icetai.assist.service.MicroServiceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.zip.ZipOutputStream;

@Slf4j
@Controller
@Validated
public class MicroServiceController {

  @Resource MicroServiceService microServiceService;

  @Resource GeneratorService generatorService;

  @GetMapping("/")
  public String homePage(
      Model model,
      @RequestParam(value = "page", required = false) Integer currentPage,
      @RequestParam(value = "size", required = false) Integer pageSize,
      @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword) {
    if (currentPage == null || currentPage <= 0) currentPage = 1;
    if (pageSize == null || pageSize <= 5) pageSize = 5;
    PageRequest pageRequest = PageRequest.of(currentPage - 1, pageSize, Sort.by("createdAt"));
    Page<MicroService> microServicePage = microServiceService.findAllPage(pageRequest, keyword);
    model.addAttribute("microServices", microServicePage);
    model.addAttribute("keyword", keyword);
    int totalPages = microServicePage.getTotalPages();
    if (totalPages > 0) {
      List<Integer> pageNumbers =
          IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
      model.addAttribute("pageNumbers", pageNumbers);
    }
    return "index";
  }

  @GetMapping("add")
  public String addPage(MicroService microService, Model model) {
    model.addAttribute("microService", microService);
    return "add-service";
  }

  @PostMapping("/add")
  public String addService(
      @Validated(OnCreate.class) MicroService microService, BindingResult result, Model model) {
    if (result.hasErrors()) {
      return "add-service";
    }

    microServiceService.save(microService);
    return "redirect:/";
  }

  @PostMapping("/update/{id}")
  public String updateService(
      @PathVariable("id") long id,
      @Validated(OnUpdate.class) MicroService microService,
      BindingResult result,
      Model model) {
    if (result.hasErrors()) {
      microService.setId(id);
      return "edit-service";
    }

    microServiceService.save(microService);
    return "redirect:/";
  }

  @GetMapping("/edit/{id}")
  public String editService(@PathVariable("id") long id, Model model) {
    MicroService edit =
        microServiceService
            .findById(id)
            .orElseThrow(() -> new IllegalArgumentException("Invalid service Id:" + id));
    model.addAttribute("microService", edit);
    return "edit-service";
  }

  @GetMapping("/delete/{id}")
  public String deleteService(@PathVariable("id") long id, Model model) {
    MicroService microService =
        microServiceService
            .findById(id)
            .orElseThrow(() -> new IllegalArgumentException("Invalid service Id:" + id));
    microServiceService.delete(microService);
    return "redirect:/";
  }

  @GetMapping(value = "/gen/{id}", produces = "application/zip")
  public void generateCode(@PathVariable("id") long id, HttpServletResponse response)
      throws IOException {

    MicroService service =
        microServiceService
            .findById(id)
            .orElseThrow(() -> new IllegalArgumentException("Invalid service Id:" + id));
    String serviceName = service.getName();
    String filename = serviceName.concat(".zip");

    String[] tableNames = service.getTableNames().split(",");

    // setting headers
    response.setStatus(HttpServletResponse.SC_OK);
    response.addHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

    ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());

    String url =
        "jdbc:mysql://10.10.202.205:3306/"
            .concat(service.getIdentification())
            .concat("?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC");
    String username = "root";
    String password = "123456";

    generatorService.generateZip(service, url, username, password, tableNames, zipOutputStream);
  }
}
